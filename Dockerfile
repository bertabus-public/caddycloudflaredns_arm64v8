FROM caddy:builder AS builder

#RUN export CGO_ENABLED=1
RUN export GOOS=linux
RUN export GOARCH=arm64
RUN xcaddy build \
    --with github.com/caddy-dns/cloudflare

FROM arm64v8/caddy

COPY --from=builder /usr/bin/caddy /usr/bin/caddy

##
## syntax=docker/dockerfile:1
#FROM docker.elastic.co/beats-dev/golang-crossbuild:1.21.4-arm
#
#RUN apt-get update && \
#    apt-get install -y libusb-1.0-0-dev:arm64 libudev-dev:arm64 && \
#    rm -rf /var/lib/apt/lists/*
#RUN go get github.com/sstallion/go-hid@latest
#docker run -it --rm \
#        -v $PWD:/go/src/github.com/user/go-project \
#        -w /go/src/github.com/user/go-project \
#        -e CGO_ENABLED=1 \
#        bertabus/arm_builder \
#        --build-cmd "go build" \
#        -p "linux/arm64"